package com.iacovelli.avaliacaoneon;

import com.iacovelli.avaliacaoneon.Data.Person;
import com.iacovelli.avaliacaoneon.Data.TokenService;
import com.iacovelli.avaliacaoneon.Main.MainContract;
import com.iacovelli.avaliacaoneon.Main.MainPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.atLeast;

/**
 * Created by douglasiacovelli on 7/08/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainTest {
    @Mock
    private MainContract.View view;

    @Mock
    private TokenService service;
    private MainPresenter presenter;

    @Before
    public void setUp() throws Exception {
        this.presenter = new MainPresenter(view, service);
    }

    @Mock
    Person user;
    @Captor
    ArgumentCaptor<Callback<String>> captor;
    @Test
    public void shouldGetTokenAndSetItOnView() throws Exception {

        // A method inside getToken will call "view.getCurrentUser()"
        // so it must return a mocked user
        when(view.getCurrentUser()).thenReturn(user);

        //Call the method to be tested
        presenter.getToken();

        // Verify that the method getToken is called with anyString as parameters for Name and Email
        // and add an argumentCaptor, which will be the callback.
        verify(service).getToken(anyString(),anyString(), captor.capture());
        Response<String> response = Response.success("001122");

        // This way we can mock the data into the callback function, such as the response created above.
        captor.getValue().onResponse(null, response);

        // And finally the test to check that the callback calls setToken when the data is correct
        verify(view).setToken("001122");
    }

    @Test
    public void shouldGetTokenAndIfFailTryToObtainTokenAgain() throws Exception {
        when(view.getCurrentUser()).thenReturn(user);
        presenter.getToken();
        verify(service).getToken(anyString(),anyString(), captor.capture());
        Response<String> response = Response.success("");

        captor.getValue().onResponse(null, response);
        verify(service, atLeast(2)).getToken(anyString(), anyString(), (Callback<String>) any());
    }

    @Test
    public void shouldLoadCurrentUserToTheView() throws Exception {
        presenter.loadCurrentUser();
        verify(view).setCurrentUser((Person) any());
    }

    @Test
    public void shouldUpdateUIInformation() throws Exception {
        when(view.getCurrentUser()).thenReturn(user);
        presenter.updatePersonInformationOnUI();

        verify(view).setName(anyString());
        verify(view).setEmail(anyString());
        verify(view).setPhoto(anyString());
    }


}
