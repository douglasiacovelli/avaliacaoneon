package com.iacovelli.avaliacaoneon;

import com.iacovelli.avaliacaoneon.Data.Person;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by douglasiacovelli on 7/08/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class PersonTest {

    @Test
    public void shouldGetInitialsCorrectly() throws Exception {
        Person person = new Person();
        person.setName("Douglas Iacovelli");
        assertEquals("DI", person.getInitials());

        person.setName("Daiane dos Santos");
        assertEquals(person.getInitials(), "DS");

        person.setName("Artur Zanetti Pereira");
        assertEquals(person.getInitials(), "AZ");

    }




}
