package com.iacovelli.avaliacaoneon.Data;

import retrofit2.Call;
import retrofit2.Callback;

import static com.iacovelli.avaliacaoneon.Data.APIService.retrofit;

/**
 * Created by douglasiacovelli on 4/08/2016.
 */

public class TokenService {
    public void getToken(String name, String email, Callback<String> callback) {
        APIService api = retrofit.create(APIService.class);

        Call<String> tokenCall = api.generateToken(name, email);
        tokenCall.enqueue(callback);
    }
}
