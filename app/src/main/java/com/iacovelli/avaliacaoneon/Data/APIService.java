package com.iacovelli.avaliacaoneon.Data;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by douglasiacovelli on 6/08/2016.
 */

public interface APIService {

    @GET("/GenerateToken")
    Call<String> generateToken(@Query("nome") String name, @Query("email") String email);

    @FormUrlEncoded
    @POST("/SendMoney")
    Call<Boolean> sendMoney(@Field("clientID") String clientID, @Field("token") String token, @Field("valor") double valor);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://processoseletivoneon.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
