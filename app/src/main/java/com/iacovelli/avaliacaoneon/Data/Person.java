package com.iacovelli.avaliacaoneon.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by douglasiacovelli on 4/08/2016.
 */

public class Person implements Parcelable {
    private String id;
    private String name;
    private String phone;
    private String photoURL;
    private String email;

    public Person() {}

    public Person(String id, String name, String phone, String photoURL) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.photoURL = photoURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getInitials() {
        String[] nameSplit = this.getName().split(" ");
        String initials = String.valueOf(nameSplit[0].charAt(0));
        if(nameSplit.length > 1) {
            if(nameSplit[1].contains("do") || nameSplit[1].contains("da")){
                initials += String.valueOf(nameSplit[2].charAt(0));
            }else {
                initials += String.valueOf(nameSplit[1].charAt(0));
            }
        }
        return initials.toUpperCase();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeString(this.photoURL);
        dest.writeString(this.email);
    }

    protected Person(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.phone = in.readString();
        this.photoURL = in.readString();
        this.email = in.readString();
    }

    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel source) {
            return new Person(source);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}
