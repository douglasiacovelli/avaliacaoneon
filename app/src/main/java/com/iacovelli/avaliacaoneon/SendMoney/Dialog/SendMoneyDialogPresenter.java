package com.iacovelli.avaliacaoneon.SendMoney.Dialog;

import com.iacovelli.avaliacaoneon.Data.Person;
import com.iacovelli.avaliacaoneon.R;
import com.iacovelli.avaliacaoneon.SendMoney.SendMoneyService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by douglasiacovelli on 10/08/2016.
 */
public class SendMoneyDialogPresenter implements SendMoneyDialogContract.Presenter {
    private final SendMoneyDialogContract.View view;
    private final SendMoneyService service;

    public SendMoneyDialogPresenter(SendMoneyDialogContract.View view, SendMoneyService service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void sendMoney() {


        String token = view.getToken();
        Float amount = view.getAmount();
        String receiverId = view.getReceiver().getId();

        if(amount <= 0 ){
            view.showError(R.string.amount_minimum_value);
            return;
        }
        view.showLoadingIndicator();
        Callback<Boolean> callback = new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                view.hideLoadingIndicator();

                if(response.isSuccessful() && response.body()) {
                    view.showFeedbackTransaction(R.string.successful_transaction);
                } else {
                    view.showInputElements();
                    view.showFeedbackTransaction(R.string.error_during_transaction);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                view.showInputElements();
                view.showFeedbackTransaction(R.string.error_during_transaction);
            }
        };

        service.sendMoney(token, amount, receiverId, callback);
    }

    @Override
    public void populateUIWithReceiverData() {
        Person receiver = view.getReceiver();
        view.setReceiverPhoto(receiver.getPhotoURL());
        view.setReceiverName(receiver.getName());
        view.setReceiverPhone(receiver.getPhone());
    }
}
