package com.iacovelli.avaliacaoneon.SendMoney;

import com.iacovelli.avaliacaoneon.Data.APIService;
import com.iacovelli.avaliacaoneon.Data.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;

import static com.iacovelli.avaliacaoneon.Data.APIService.retrofit;

/**
 * Created by douglasiacovelli on 8/08/2016.
 */
public class SendMoneyService {
    ArrayList<Person> loadContacts() {
        ArrayList<Person> contacts = new ArrayList<>();
        String[] names = {"Marta", "Sarah Menezes", "Arthur Zanetti", "Giba", "Fernanda Garay", "Roberto Carlos", "César Cielo", "Vanderlei de Lmima", "Alison Cerutti", "Maurren Maggi", "Daiane dos Santos", "Diego Hipólito", "Yamaguchi Falcão", "Neymar dos Santos", "Ronaldinho Gaúcho"};
        String[] phones = {"(11) 91234-5678", "(23) 92345-9909", "(19) 92900-2903", "(11) 92050-8878", "(21) 98292-8122", "(21) 95502-9098", "(13) 99829-8001", "(11) 91283-1923", "(11) 99128-8922", "(35) 90392-8721", "(21) 99882-4422", "(13) 93920-2020", "(11) 98292-8121", "(11) 92823-0031", "(21) 93108-1912"};
        for(int i = 0; i < names.length; i++){
            Person contact = new Person();
            contact.setId(i+ "");
            contact.setName(names[i]);
            contact.setEmail(names[i].replace(" ", ".") + "@"+ "olimpicos.com.br");
            contact.setPhone(phones[i]);
            contacts.add(contact);
        }
        contacts.get(0).setPhotoURL("http://trivela.uol.com.br/wp-content/uploads/2016/02/marta-1260x710.jpg");
        contacts.get(2).setPhotoURL("http://ig-wp-colunistas.s3.amazonaws.com/esportesolimpicos/wp-content/uploads/2013/10/Zanetti_CBG.jpg");
        contacts.get(3).setPhotoURL("http://www.volleywood.net/wp-content/uploads/2014/08/Gilberto-Amauri-de-Godoy-Filho-aka-Giba-2.jpg");
        contacts.get(6).setPhotoURL("http://www.bestswim.com.br/wp-content/uploads/2016/08/Cielo.jpg");

        Collections.sort(contacts, new Comparator<Person>() {
            @Override
            public int compare(Person person1, Person person2) {
                return person1.getName().compareTo(person2.getName());
            }
        });
        return contacts;
    }

    public void sendMoney(String token, Float amount, String receiverId, Callback<Boolean> callback) {
        APIService api = retrofit.create(APIService.class);
        Call<Boolean> sendMoney = api.sendMoney(receiverId, token, amount);
        sendMoney.enqueue(callback);
    }
}
