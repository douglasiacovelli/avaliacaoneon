package com.iacovelli.avaliacaoneon.SendMoney;


import com.iacovelli.avaliacaoneon.Data.Person;

import java.util.ArrayList;

/**
 * Created by douglasiacovelli on 8/08/2016.
 */
public class SendMoneyPresenter implements SendMoneyContract.Presenter {

    private SendMoneyContract.View view;
    private SendMoneyService service;

    public SendMoneyPresenter(SendMoneyContract.View view, SendMoneyService service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void loadContacts() {
        ArrayList<Person> contacts = service.loadContacts();
        view.updateContactList(contacts);
    }
}
