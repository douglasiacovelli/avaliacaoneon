package com.iacovelli.avaliacaoneon.SendMoney;

import com.iacovelli.avaliacaoneon.Data.Person;

import java.util.ArrayList;

/**
 * Created by douglasiacovelli on 8/08/2016.
 */
public interface SendMoneyContract {
    interface View {
        String getToken();
        void updateContactList(ArrayList<Person> contacts);
        void showSendMoneyDialog(Person person);
    }

    interface Presenter {

        void loadContacts();
    }
}
