package com.iacovelli.avaliacaoneon.SendMoney.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.iacovelli.avaliacaoneon.Data.Person;
import com.iacovelli.avaliacaoneon.R;
import com.iacovelli.avaliacaoneon.SendMoney.SendMoneyService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.seismic.ShakeDetector;

import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by douglasiacovelli on 9/08/2016.
 */
public class SendMoneyDialog extends DialogFragment implements SendMoneyDialogContract.View, ShakeDetector.Listener  {

    private Button buttonSend;
    private CircleImageView profilePicture;
    private TextView textName;
    private TextView textPhone;
    private CurrencyEditText editTextAmount;
    private String token;
    private SendMoneyDialogContract.Presenter presenter;
    private Person receiver;
    private ImageLoader imageLoader;
    private ProgressBar loadingIndicator;
    private TextView textAmountLabel;
    private TextView textTransactionFeedback;
    private TextView textShakeToSend;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_send_money, null);
        builder.setView(view);

        this.token = getArguments().getString("token");
        this.receiver = getArguments().getParcelable("receiver");
        this.imageLoader = ImageLoader.getInstance();

        this.getUIElementsReferences(view);
        this.setupUIElements();
        this.setupShakeDetector();

        presenter = new SendMoneyDialogPresenter(this, new SendMoneyService());
        presenter.populateUIWithReceiverData();

        return builder.create();
    }

    private void setupShakeDetector() {
        SensorManager sensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(this);
        sd.start(sensorManager);
    }

    private void getUIElementsReferences(View view) {
        this.profilePicture = (CircleImageView) view.findViewById(R.id.profile_picture);
        this.textName = (TextView) view.findViewById(R.id.text_name);
        this.textPhone = (TextView) view.findViewById(R.id.text_phone);

        this.editTextAmount = (CurrencyEditText) view.findViewById(R.id.edittext_amount);
        this.buttonSend = (Button) view.findViewById(R.id.button_send);
        this.textAmountLabel = (TextView) view.findViewById(R.id.text_amount_label);
        this.textTransactionFeedback = (TextView) view.findViewById(R.id.text_transaction_feedback);
        this.loadingIndicator = (ProgressBar) view.findViewById(R.id.loading_indicator);
        this.textShakeToSend = (TextView) view.findViewById(R.id.text_shake_to_send);
    }

    private void setupUIElements() {
        editTextAmount.setLocale(new Locale("pt", "BR"));
        editTextAmount.setText(editTextAmount.formatCurrency(Long.toString(0)));
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.sendMoney();
            }
        };
        buttonSend.setOnClickListener(onClickListener);
    }

    @Override
    public String getToken() {
        return this.token;
    }

    @Override
    public Float getAmount() {
        Long rawVal = this.editTextAmount.getRawValue();
        return rawVal.floatValue()/100;
    }

    @Override
    public Person getReceiver() {
        return this.receiver;
    }

    @Override
    public void setReceiverPhoto(String photoURL) {
        if(photoURL == null){
            this.profilePicture.setVisibility(View.GONE);
        }else {
            this.imageLoader.displayImage(
                    photoURL,
                    this.profilePicture,
                    new DisplayImageOptions.Builder()
                            .cacheInMemory(true)
                            .build()
            );
        }
    }

    @Override
    public void setReceiverName(String name) {
        this.textName.setText(name);
    }

    @Override
    public void setReceiverPhone(String phone) {
        this.textPhone.setText(phone);
    }

    @Override
    public void showError(int resId) {
        this.editTextAmount.setError(getActivity().getString(resId));
    }

    @Override
    public void showLoadingIndicator() {
        this.profilePicture.setVisibility(View.GONE);
        this.textName.setVisibility(View.GONE);
        this.textPhone.setVisibility(View.GONE);
        this.editTextAmount.setVisibility(View.GONE);
        this.textAmountLabel.setVisibility(View.GONE);
        this.buttonSend.setVisibility(View.GONE);
        this.textShakeToSend.setVisibility(View.GONE);
        this.loadingIndicator.setVisibility(View.VISIBLE);
    }
    @Override
    public void showInputElements() {
        this.profilePicture.setVisibility(View.VISIBLE);
        this.textName.setVisibility(View.VISIBLE);
        this.textPhone.setVisibility(View.VISIBLE);
        this.editTextAmount.setVisibility(View.VISIBLE);
        this.textAmountLabel.setVisibility(View.VISIBLE);
        this.buttonSend.setVisibility(View.VISIBLE);
        this.textTransactionFeedback.setVisibility(View.GONE);
    }

    @Override
    public void showFeedbackTransaction(int resId) {
        if(resId == R.string.successful_transaction) {
            this.textTransactionFeedback.setTextColor(Color.parseColor("#449944"));
        } else {
            this.textTransactionFeedback.setTextColor(Color.parseColor("#C90808"));
        }
        this.textTransactionFeedback.setText(getActivity().getString(resId));
        this.textTransactionFeedback.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingIndicator() {
        this.loadingIndicator.setVisibility(View.GONE);
    }


    @Override
    public void hearShake() {
        presenter.sendMoney();
    }
}
