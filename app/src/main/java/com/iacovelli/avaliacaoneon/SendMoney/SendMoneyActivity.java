package com.iacovelli.avaliacaoneon.SendMoney;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.iacovelli.avaliacaoneon.Data.Person;
import com.iacovelli.avaliacaoneon.R;
import com.iacovelli.avaliacaoneon.SendMoney.Dialog.SendMoneyDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class SendMoneyActivity extends AppCompatActivity implements SendMoneyContract.View {

    private String token;
    private SendMoneyContract.Presenter presenter;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ContactListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);

        this.token = getIntent().getStringExtra("token");
        this.presenter = new SendMoneyPresenter(this, new SendMoneyService());

        this.setupList();
        presenter.loadContacts();
    }

    private void setupList() {
        recyclerView = (RecyclerView) findViewById(R.id.list_contacts);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ImageLoader imageLoader = ImageLoader.getInstance();
        // specify an adapter (see also next example)
        mAdapter = new ContactListAdapter(new ArrayList<Person> (), imageLoader, this);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public String getToken() {
        return token;
    }

    public void updateContactList(ArrayList<Person> contacts) {
        this.mAdapter.swap(contacts);
    }

    @Override
    public void showSendMoneyDialog(Person person) {
        DialogFragment dialog = new SendMoneyDialog();

        Bundle args = new Bundle();
        args.putString("token", this.token);
        args.putParcelable("receiver", person);

        dialog.setArguments(args);
        dialog.show(getFragmentManager(), "sendMoneyDialog");
    }
}
