package com.iacovelli.avaliacaoneon.SendMoney.Dialog;

import com.iacovelli.avaliacaoneon.Data.Person;

/**
 * Created by douglasiacovelli on 10/08/2016.
 */
public interface SendMoneyDialogContract {
    interface View{
        String getToken();
        Float getAmount();

        Person getReceiver();

        void setReceiverPhoto(String photoURL);
        void setReceiverName(String name);
        void setReceiverPhone(String phone);
        void showError(int resId);

        void showLoadingIndicator();
        void hideLoadingIndicator();
        void showInputElements();
        void showFeedbackTransaction(int resId);
    }

    interface Presenter {
        void sendMoney();
        void populateUIWithReceiverData();
    }

}
