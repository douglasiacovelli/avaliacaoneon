package com.iacovelli.avaliacaoneon.SendMoney;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iacovelli.avaliacaoneon.Data.Person;
import com.iacovelli.avaliacaoneon.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by douglasiacovelli on 9/08/2016.
 */
public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    private final ArrayList<Person> contacts;
    private final ImageLoader imageLoader;
    private final SendMoneyContract.View sendMoneyView;
    private RecyclerView recyclerView;

    public ContactListAdapter(ArrayList<Person> contacts, ImageLoader imageLoader, SendMoneyContract.View sendMoneyView) {
        this.contacts = contacts;
        this.imageLoader = imageLoader;
        this.sendMoneyView = sendMoneyView;
    }

    public void swap(ArrayList<Person> contacts) {
        this.contacts.clear();
        this.contacts.addAll(contacts);
        notifyDataSetChanged();
    }

    public void toggleProfilePictureAndPlaceholder(View placeholder, View image, boolean isTherePhoto) {
        if(isTherePhoto){
            placeholder.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }else {
            placeholder.setVisibility(View.VISIBLE);
            image.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person contact = contacts.get(position);
        holder.textName.setText(contact.getName());
        holder.textPhone.setText(contact.getPhone());

        toggleProfilePictureAndPlaceholder(holder.placeholderProfilePicture, holder.imageProfile, false);
        holder.placeholderInitial.setText(contact.getInitials());

        String photoURL = contact.getPhotoURL();
        if(photoURL != null) {
            toggleProfilePictureAndPlaceholder(holder.placeholderProfilePicture, holder.imageProfile, true);
            imageLoader.displayImage(photoURL, holder.imageProfile);
            imageLoader.displayImage(
                    photoURL,
                    holder.imageProfile,
                    new DisplayImageOptions.Builder()
                            .cacheInMemory(true)
                            .cacheOnDisk(true)
                            .build()
            );
        }

    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int position = recyclerView.getChildLayoutPosition(view);
            sendMoneyView.showSendMoneyDialog(contacts.get(position));
        }
    };
    // Create new views (invoked by the layout manager)
    @Override
    public ContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        recyclerView = (RecyclerView) parent;
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contacts, parent, false);
        // set the view's size, margins, paddings and layout parameters
        v.setOnClickListener(onClickListener);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {


        // each data item is just a string in this case
        public CircleImageView imageProfile;
        public TextView textName;
        public TextView textPhone;
        public LinearLayout placeholderProfilePicture;
        public TextView placeholderInitial;


        public ViewHolder(View v) {
            super(v);
            imageProfile = (CircleImageView) v.findViewById(R.id.image_profile_picture);
            textName = (TextView) v.findViewById(R.id.text_name);
            textPhone = (TextView) v.findViewById(R.id.text_phone);
            placeholderProfilePicture = (LinearLayout) v.findViewById(R.id.placeholder_profile_picture);
            placeholderInitial = (TextView) v.findViewById(R.id.placeholder_initial);
        }
    }
}
