package com.iacovelli.avaliacaoneon.Main;

import com.iacovelli.avaliacaoneon.Data.Person;
import com.iacovelli.avaliacaoneon.Data.TokenService;
import com.iacovelli.avaliacaoneon.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by douglasiacovelli on 4/08/2016.
 */

public class MainPresenter implements MainContract.Presenter {
    private final TokenService service;
    private MainContract.View view;

    public MainPresenter(MainContract.View view, TokenService service) {
        this.view = view;
        this.service = service;
    }

    public void loadCurrentUser(){
        Person currentUser = new Person();
        currentUser.setName("Douglas Iacovelli");
        currentUser.setEmail("douglas.iacovelli@gmail.com");
        currentUser.setPhotoURL("https://scontent-gru2-1.xx.fbcdn.net/t31.0-8/12473989_10207056706389944_6538422585510608751_o.jpg");
        view.setCurrentUser(currentUser);
        this.updatePersonInformationOnUI();
    }

    public void updatePersonInformationOnUI() {
        Person currentUser = view.getCurrentUser();
        view.setPhoto(currentUser.getPhotoURL());
        view.setName(currentUser.getName());
        view.setEmail(currentUser.getEmail());
    }

    public void getToken() {
        view.showProgressIndicator();
        Person currentUser = view.getCurrentUser();
        service.getToken(currentUser.getName(), currentUser.getEmail(), new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String token = response.body();
                if(!token.isEmpty()) {
                    view.setToken(token);

                    // hideProgressIndicator was called here just to give it some time to
                    // pretend it was loading the user info from the server, not only the token
                    view.hideProgressIndicator();

                } else {
                    getToken();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                view.showErrorConnection(R.string.errorFetchingData);
            }
        });
    }

    @Override
    public void clickedButtonHistory() {
        view.showHistoryOfTransactionsScreen();
    }

    @Override
    public void clickedButtonSendMoney() {
        view.showSendMoneyScreen();
    }
}
