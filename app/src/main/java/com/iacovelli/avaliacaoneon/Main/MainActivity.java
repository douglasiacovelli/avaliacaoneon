package com.iacovelli.avaliacaoneon.Main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iacovelli.avaliacaoneon.Data.Person;
import com.iacovelli.avaliacaoneon.Data.TokenService;
import com.iacovelli.avaliacaoneon.R;
import com.iacovelli.avaliacaoneon.SendMoney.SendMoneyActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainPresenter presenter;
    private TextView nameView;
    private TextView emailView;
    private CircleImageView profilePictureView;
    private ImageLoader imageLoader;
    private Person currentUser;
    private String token;
    private Button buttonSendMoney;
    private Button buttonHistoryOfTransactions;
    private ProgressBar progressIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        this.getUIElementsReferences();
        this.createListeners();
        presenter = new MainPresenter(this, new TokenService());
        presenter.loadCurrentUser();
        presenter.getToken();
    }

    public void getUIElementsReferences() {
        nameView = (TextView) findViewById(R.id.name_view);
        emailView = (TextView) findViewById(R.id.email_view);
        profilePictureView = (CircleImageView) findViewById(R.id.profile_picture_view);
        buttonSendMoney = (Button) findViewById(R.id.button_send_money);
        buttonHistoryOfTransactions = (Button) findViewById(R.id.button_history);
        progressIndicator = (ProgressBar) findViewById(R.id.progress_indicator);
    }

    public void createListeners() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()){
                    case R.id.button_history:
                        presenter.clickedButtonHistory();
                        break;
                    case R.id.button_send_money:
                        presenter.clickedButtonSendMoney();
                        break;
                }
            }
        };
        buttonHistoryOfTransactions.setOnClickListener(onClickListener);
        buttonSendMoney.setOnClickListener(onClickListener);

    }

    @Override
    public Person getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(Person person) {
        currentUser = person;
    }

    @Override
    public void setName(String name) {
        nameView.setText(name);
    }

    @Override
    public void setEmail(String email) {
        emailView.setText(email);
    }

    @Override
    public void setPhoto(String photoUrl) {
        imageLoader.loadImage(photoUrl, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                profilePictureView.setImageBitmap(loadedImage);
            }
        });
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public void showProgressIndicator() {
        this.profilePictureView.setVisibility(View.INVISIBLE);
        this.nameView.setVisibility(View.INVISIBLE);
        this.emailView.setVisibility(View.INVISIBLE);
        this.progressIndicator.setVisibility(View.VISIBLE);

        this.buttonHistoryOfTransactions.setAlpha((float) 0.5);
        this.buttonHistoryOfTransactions.setClickable(false);

        this.buttonSendMoney.setAlpha((float) 0.5);
        this.buttonSendMoney.setClickable(false);
    }

    @Override
    public void hideProgressIndicator() {
        this.profilePictureView.setVisibility(View.VISIBLE);
        this.nameView.setVisibility(View.VISIBLE);
        this.emailView.setVisibility(View.VISIBLE);
        this.progressIndicator.setVisibility(View.INVISIBLE);

        this.buttonHistoryOfTransactions.setAlpha(1);
        this.buttonHistoryOfTransactions.setClickable(true);

        this.buttonSendMoney.setAlpha(1);
        this.buttonSendMoney.setClickable(true);
    }

    public void showSendMoneyScreen(){
        Intent intent = new Intent(this, SendMoneyActivity.class);
        intent.putExtra("token", this.token);
        startActivity(intent);
    }

    @Override
    public void showHistoryOfTransactionsScreen() {
        //TODO: implement new Intent to Activity HistoryOfTransactions
    }

    @Override
    public void showErrorConnection(int resId) {
        Toast.makeText(MainActivity.this, getString(resId), Toast.LENGTH_LONG).show();
    }

}
