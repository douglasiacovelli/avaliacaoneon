package com.iacovelli.avaliacaoneon.Main;

import com.iacovelli.avaliacaoneon.Data.Person;

/**
 * Created by douglasiacovelli on 4/08/2016.
 */
public interface MainContract {
    interface View {
        Person getCurrentUser();
        void setCurrentUser(Person person);
        void setName(String name);
        void setEmail(String email);
        void setPhoto(String photoUrl);

        void setToken(String token);

        void showProgressIndicator();
        void hideProgressIndicator();

        void showSendMoneyScreen();
        void showHistoryOfTransactionsScreen();

        void showErrorConnection(int resId);
    }

    interface Presenter {
        void loadCurrentUser();
        void updatePersonInformationOnUI();

        void clickedButtonHistory();
        void clickedButtonSendMoney();
    }
}
